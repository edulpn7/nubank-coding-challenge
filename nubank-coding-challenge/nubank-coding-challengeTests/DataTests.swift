//
//  DataTests.swift
//  nubank-coding-challengeTests
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import XCTest
@testable import nubank_coding_challenge
import RxNimble
import Nimble
import RxSwift

func match(URL expectedValue: String) -> Predicate<ResourceLinkEntity> {
    return Predicate.define("matchURL <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.href == expectedValue, message: msg)
    }
}

func match(id expectedId: String, title expectedTitle :String) -> Predicate<ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity> {
    return Predicate.define("match id and title") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: (actualValue.id == expectedId) && (actualValue.title == expectedTitle), message: msg)
    }
}

func match(primaryActionType expectedValue: NoticeResource.NoticeResourceAction.NoticeResourceActionType) -> Predicate<NoticeResource> {
    return Predicate.define("matchPrimaryActionType <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.primaryAction.action == expectedValue, message: msg)
    }
}

func match(secondaryActionType expectedValue: NoticeResource.NoticeResourceAction.NoticeResourceActionType) -> Predicate<NoticeResource> {
    return Predicate.define("matchSecondaryActionType <\(stringify(expectedValue))>") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.secondaryAction.action == expectedValue, message: msg)
    }
}

class DataTests: XCTestCase {
    struct FakeNoticeResourceDataSource: NoticeResourceDataSourceProtocol {
        let primaryAction: NoticeResourceEntity.NoticeResourceActionEntity
        let secondaryAction: NoticeResourceEntity.NoticeResourceActionEntity
        static let fakeURLString = "https://nubank.com.br"
        
        func retrieveNoticeResourceEntity(from href: URL) -> Observable<NoticeResourceEntity> {
            return Observable.just(NoticeResourceEntity(title: "", description: "", primaryAction: primaryAction, secondaryAction: secondaryAction, links: NoticeResourceEntity.NoticeResourceLinksEntity(chargebackLink: ResourceLinkEntity(href: FakeNoticeResourceDataSource.fakeURLString))))
        }
    }
    
    func testParseNoticeAction() {
        let fakeValidDataSource = FakeNoticeResourceDataSource(primaryAction: NoticeResourceEntity.NoticeResourceActionEntity(title: "title", action: "continue"), secondaryAction: NoticeResourceEntity.NoticeResourceActionEntity(title: "title", action: "cancel"))
        let fakeInvalidDataSource = FakeNoticeResourceDataSource(primaryAction: NoticeResourceEntity.NoticeResourceActionEntity(title: "title", action: "unknown"), secondaryAction: NoticeResourceEntity.NoticeResourceActionEntity(title: "title", action: "unknown"))
        let validRepository = NoticeResourceRepository(dataSource: fakeValidDataSource)
        let invalidRepository = NoticeResourceRepository(dataSource: fakeInvalidDataSource)
        
        expect(validRepository.retrieveNoticeResource(from: URL(string: FakeNoticeResourceDataSource.fakeURLString)!)).first.to(match(primaryActionType: .proceed))
        expect(validRepository.retrieveNoticeResource(from: URL(string: FakeNoticeResourceDataSource.fakeURLString)!)).first.to(match(secondaryActionType: .cancel))
        
        expect(invalidRepository.retrieveNoticeResource(from: URL(string: FakeNoticeResourceDataSource.fakeURLString)!)).first.to(match(primaryActionType: .unknown))
        expect(invalidRepository.retrieveNoticeResource(from: URL(string: FakeNoticeResourceDataSource.fakeURLString)!)).first.to(match(secondaryActionType: .unknown))
    }
    
    func testResourceLinkParsing() {
        let decoder = JSONDecoder()
        if let jsonFail1 = "{}".data(using: .utf8),
            let jsonFail2 = "{\"href\" = 12}".data(using: .utf8),
            let jsonSuccess = "{\"href\": \"url.com\"}".data(using: .utf8) {
            expect(try decoder.decode(ResourceLinkEntity.self, from: jsonFail1)).to(throwError())
            expect(try decoder.decode(ResourceLinkEntity.self, from: jsonFail2)).to(throwError())
            expect(try decoder.decode(ResourceLinkEntity.self, from: jsonSuccess)).to(match(URL:"url.com"))
        } else {
            fail()
        }
    }
    
    func testReasonDetailsParsing() {
        let decoder = JSONDecoder()
        if let jsonFail1 = "{}".data(using: .utf8),
            let jsonFail2 = "{\"id\" = \"id\"}".data(using: .utf8),
            let jsonSuccess = "{\"id\": \"id\", \"title\": \"title\"}".data(using: .utf8) {
            expect(try decoder.decode(ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity.self, from: jsonFail1)).to(throwError())
            expect(try decoder.decode(ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity.self, from: jsonFail2)).to(throwError())
            expect(try decoder.decode(ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity.self, from: jsonSuccess)).to(match(id: "id", title: "title"))
        } else {
            fail()
        }
    }
}
