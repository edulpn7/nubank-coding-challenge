//
//  DomainTests.swift
//  nubank-coding-challengeTests
//
//  Created by Eduardo Pinto on 4/22/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import XCTest
@testable import nubank_coding_challenge
import RxNimble
import Nimble
import RxSwift

func match(id expectedId: String) -> Predicate<ChargebackResource> {
    return Predicate.define("match id") { actualExpression, msg in
        guard let actualValue = try actualExpression.evaluate() else {
            return PredicateResult(status: .fail, message: msg)
        }
        return PredicateResult(bool: actualValue.id == expectedId, message: msg)
    }
}

class DomainTests: XCTestCase {
    class FakeAutoblockTestChargebackRepository: ChargebackResourceRepositoryProtocol {
        var hasAutoblocked: Bool = false
        
        func performChargeback(from href: URL, with comment: String, and reasonDetails: [(id: String, response: Bool)]) -> Observable<ChargebackResult> {
            return Observable.error(HTTPError.badRequest)
        }
        
        func unblockCard(at href: URL) -> Observable<CardOperationResult> {
            return Observable.error(HTTPError.badRequest)
        }
        
        func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResource> {
            let model = ChargebackResource(commentHint: "", id: "test", title: "", shouldAutoblock: true, reasonDetails: [], links: ChargebackResource.ChargebackResourceLinks(blockCardLink: ResourceLink(href: href) , unblockCardLink: ResourceLink(href: href), selfLink: ResourceLink(href: href)))
            return Observable.just(model)
        }
        
        func blockCard(at href: URL) -> Observable<CardOperationResult> {
            hasAutoblocked = true
            let result = CardOperationResult(isBlocked: true, status: "ok")
            return Observable.just(result)
        }
    }
    
    func testAutoblock() {
        let repository = FakeAutoblockTestChargebackRepository()
        let useCase = RetrieveChargebackResourceUseCase(repository: repository)
        expect(useCase.retrieveChargebackResource(from: URL(string: "anyurl.com")!)).first.to(match(id: "test"))
        expect(repository.hasAutoblocked).to(beTrue())
    }
}
