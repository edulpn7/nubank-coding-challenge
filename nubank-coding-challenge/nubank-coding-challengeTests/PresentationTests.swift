//
//  PresentationTests.swift
//  nubank-coding-challengeTests
//
//  Created by Eduardo Pinto on 4/23/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import XCTest
@testable import nubank_coding_challenge
import RxNimble
import Nimble
import RxSwift

class PresentationTests: XCTestCase {
    class StubNoticeView: NoticeViewProtocol {
        var presenter: NoticePresenterProtocol!
        var router: NoticeRouterProtocol!
        var loadingView: UIView!
        
        var hasCalledDisplayResource = false
        var hasCalledDisplayError = false
        var hasCalledStartLoading = false
        var hasCalledStopLoading = false
        
        func display(_ noticeResourceViewModel: NoticeResourceViewModel) {
            hasCalledDisplayResource = true
        }
        
        func display(_ error: Error) {
            hasCalledDisplayError = true
        }
        
        func startLoading() {
            hasCalledStartLoading = true
        }
        
        func stopLoading() {
            hasCalledStopLoading = true
        }
    }
    
    struct FakeSuccessRetrieveNoticeResourceUseCase: RetrieveNoticeResourceUseCaseProtocol {
        func retrieveNoticeResource() -> Observable<NoticeResource> {
            return Observable.just(NoticeResource(title: "", description: "<p>Estamos com você nesta! Certifique-se dos pontos abaixo, são muito importantes:<br/><strong>• Você pode <font color=\"#6e2b77\">procurar o nome do estabelecimento no Google</font>. Diversas vezes encontramos informações valiosas por lá e elas podem te ajudar neste processo.</strong><br/><strong>• Caso você reconheça a compra, é muito importante pra nós que entre em contato com o estabelecimento e certifique-se que a situação já não foi resolvida.</strong></p>", primaryAction: NoticeResource.NoticeResourceAction.init(title: "", action: .cancel), secondaryAction: NoticeResource.NoticeResourceAction.init(title: "", action: .proceed), links: NoticeResource.NoticeResourceLinks(chargebackLink: ResourceLink(href: URL(string:"anyurl.com")!))))
        }
    }
    
    struct FakeErrorRetrieveNoticeResourceUseCase: RetrieveNoticeResourceUseCaseProtocol {
        func retrieveNoticeResource() -> Observable<NoticeResource> {
            return Observable.error(HTTPError.badRequest)
        }
    }
    
    //Strong references so it doesn't get dealocated while waiting for async calls
    let errorView = StubNoticeView()
    let successView = StubNoticeView()
    
    func testNoticePresenter() {
        let errorUseCase = FakeErrorRetrieveNoticeResourceUseCase()
        let successUseCase = FakeSuccessRetrieveNoticeResourceUseCase()
        let errorPresenter = NoticePresenter()
        let successPresenter = NoticePresenter()
        
        errorPresenter.retrieveNoticeResourceUseCase = errorUseCase
        errorPresenter.view = errorView
        errorView.presenter = errorPresenter
        successPresenter.retrieveNoticeResourceUseCase = successUseCase
        successPresenter.view = successView
        successView.presenter = successPresenter
        
        successPresenter.retrieveNotice()
        errorPresenter.retrieveNotice()
        
        expect(self.successView.hasCalledDisplayResource).toEventually(beTrue())
        expect(self.successView.hasCalledDisplayError).toEventually(beFalse())
        expect(self.successView.hasCalledStartLoading).toEventually(beTrue())
        expect(self.successView.hasCalledStopLoading).toEventually(beTrue())
        
        expect(self.errorView.hasCalledDisplayResource).toEventually(beFalse())
        expect(self.errorView.hasCalledDisplayError).toEventually(beTrue())
        expect(self.errorView.hasCalledStartLoading).toEventually(beTrue())
        expect(self.errorView.hasCalledStopLoading).toEventually(beTrue())
    }
}
