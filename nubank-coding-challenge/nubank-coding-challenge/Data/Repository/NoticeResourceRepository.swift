//
//  NoticeResourceRepository.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct NoticeResourceRepository: NoticeResourceRepositoryProtocol {
    let dataSource: NoticeResourceDataSourceProtocol
    
    func retrieveNoticeResource(from href: URL) -> Observable<NoticeResource> {
        return dataSource.retrieveNoticeResourceEntity(from: href).mapNoticeResource()
    }
}
