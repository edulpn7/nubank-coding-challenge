//
//  NoticeResourceRepositoryProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol NoticeResourceRepositoryProtocol {
    func retrieveNoticeResource(from href: URL) -> Observable<NoticeResource>
}
