//
//  ChargebackResourceRepositoryProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol ChargebackResourceRepositoryProtocol {
    func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResource>
    func performChargeback(from href: URL, with comment: String, and reasonDetails: [(id: String, response: Bool)]) -> Observable<ChargebackResult>
    func blockCard(at href: URL) -> Observable<CardOperationResult>
    func unblockCard(at href: URL) -> Observable<CardOperationResult>
}
