//
//  ChargebackResourceRepository.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct ChargebackResourceRepository: ChargebackResourceRepositoryProtocol {
    let dataSource: ChargebackResourceDataSourceProtocol
    let ramDataSource: ChargebackResourceRAMDataSourceProtocol
    
    func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResource> {
        if let entity = ramDataSource.storedData {
            return Observable.just(entity).mapChargebackResource()
        } else {
            return dataSource.retrieveChargebackResource(from: href).do(onNext: { entity in
                self.ramDataSource.storedData = entity
            }).mapChargebackResource()
        }
    }
    
    func performChargeback(from href: URL, with comment: String, and reasonDetails: [(id: String, response: Bool)]) -> Observable<ChargebackResult> {
        return dataSource.performChargeback(from: href, with: comment, and: reasonDetails).mapChargebackResult()
    }
    
    func blockCard(at href: URL) -> Observable<CardOperationResult> {
        return dataSource.blockCard(at: href).mapCardOperationResult(isBlockOperation: true)
    }
    
    func unblockCard(at href: URL) -> Observable<CardOperationResult> {
        return dataSource.unblockCard(at: href).mapCardOperationResult(isBlockOperation: false)
    }
}
