//
//  ObservableType+RootResourceMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

extension RootResource {
    init(mapping entity: RootResourceEntity) throws {
        links = try RootResourceLinks(mapping: entity.links)
    }
}

extension RootResource.RootResourceLinks {
    init(mapping entity: RootResourceEntity.RootResourceLinksEntity) throws {
        noticeLink = try ResourceLink(mapping: entity.noticeLink)
    }
}

extension ObservableType where E == RootResourceEntity {
    func mapRootResource() -> Observable<RootResource> {
        return flatMap { entity -> Observable<RootResource> in
            do {
                let model = try RootResource(mapping: entity)
                return Observable.just(model)
            } catch let error {
                return Observable.error(error)
            }
        }
    }
}
