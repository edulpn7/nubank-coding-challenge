//
//  ObservableType+CardOperationResultMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/18/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

extension CardOperationResult {
    init(mapping entity: CardOperationResultEntity, and isBlockOperation: Bool) {
        status = entity.status
        isBlocked = isBlockOperation
    }
}

extension ObservableType where E == CardOperationResultEntity {
    func mapCardOperationResult(isBlockOperation: Bool) -> Observable<CardOperationResult> {
        return flatMap { entity -> Observable<CardOperationResult> in
            let model = CardOperationResult(mapping: entity, and: isBlockOperation)
            return Observable.just(model)
        }
    }
}
