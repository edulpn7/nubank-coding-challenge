//
//  ObservableType+ChargebackResultMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/20/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

extension ChargebackResult {
    init(mapping entity: ChargebackResultEntity) {
        status = entity.status
    }
}

extension ObservableType where E == ChargebackResultEntity {
    func mapChargebackResult() -> Observable<ChargebackResult> {
        return flatMap { entity -> Observable<ChargebackResult> in
            let model = ChargebackResult(mapping: entity)
            return Observable.just(model)
        }
    }
}
