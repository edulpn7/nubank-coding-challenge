//
//  ObservableType+NoticeResourceMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

extension NoticeResource {
    init(mapping entity: NoticeResourceEntity) throws {
        title = entity.title
        description = entity.description
        primaryAction = NoticeResourceAction(mapping: entity.primaryAction)
        secondaryAction = NoticeResourceAction(mapping: entity.secondaryAction)
        links = try NoticeResourceLinks(mapping: entity.links)
    }
}

extension NoticeResource.NoticeResourceLinks {
    init(mapping entity: NoticeResourceEntity.NoticeResourceLinksEntity) throws {
        chargebackLink = try ResourceLink(mapping: entity.chargebackLink)
    }
}

extension NoticeResource.NoticeResourceAction {
    init(mapping entity: NoticeResourceEntity.NoticeResourceActionEntity) {
        title = entity.title
        action = NoticeResourceActionType(mapping: entity.action)
    }
}

extension NoticeResource.NoticeResourceAction.NoticeResourceActionType {
    init(mapping string: String) {
        switch string {
        case "continue":
            self = .proceed
        case "cancel":
            self = .cancel
        default:
            self = .unknown
        }
    }
}

extension ObservableType where E == NoticeResourceEntity {
    func mapNoticeResource() -> Observable<NoticeResource> {
        return flatMap { entity -> Observable<NoticeResource> in
            do {
                let model = try NoticeResource(mapping: entity)
                return Observable.just(model)
            } catch let error {
                return Observable.error(error)
            }
        }
    }
}
