//
//  ResourceLink+Mapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

extension ResourceLink {
    init(mapping entity: ResourceLinkEntity) throws {
        guard let hrefURL = URL(string: entity.href) else {
            throw StringError.couldNotMapToURL(href: entity.href)
        }
        
        href = hrefURL
    }
}
