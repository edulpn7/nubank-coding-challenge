//
//  ObservableType+ChargebackResourceMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

extension ChargebackResource {
    init(mapping entity: ChargebackResourceEntity) throws {
        commentHint = entity.commentHint
        id = entity.id
        title = entity.title
        shouldAutoblock = entity.shouldAutoblock
        reasonDetails = ChargebackResource.ChargebackResourceReasonDetail.array(from: entity.reasonDetails)
        links = try ChargebackResourceLinks(mapping: entity.links)
    }
}

extension ChargebackResource.ChargebackResourceReasonDetail {
    static func array(from entities: [ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity]) -> [ChargebackResource.ChargebackResourceReasonDetail] {
        return entities.map { entity -> ChargebackResource.ChargebackResourceReasonDetail in
            return ChargebackResource.ChargebackResourceReasonDetail(mapping: entity)
        }
    }
    
    init(mapping entity: ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity) {
        id = entity.id
        title = entity.title
    }
}

extension ChargebackResource.ChargebackResourceLinks {
    init(mapping entity: ChargebackResourceEntity.ChargebackResourceLinksEntity) throws {
        blockCardLink = try ResourceLink(mapping: entity.blockCardLink)
        unblockCardLink = try ResourceLink(mapping: entity.unblockCardLink)
        selfLink = try ResourceLink(mapping: entity.selfLink)
    }
}

extension ObservableType where E == ChargebackResourceEntity {
    func mapChargebackResource() -> Observable<ChargebackResource> {
        return flatMap { entity -> Observable<ChargebackResource> in
            do {
                let model = try ChargebackResource(mapping: entity)
                return Observable.just(model)
            } catch let error {
                return Observable.error(error)
            }
        }
    }
}
