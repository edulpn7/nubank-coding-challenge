//
//  RootResourceRepository.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct RootResourceRepository: RootResourceRepositoryProtocol {
    let dataSource: RootResourceDataSourceProtocol
    
    func retrieveRootResource() -> Observable<RootResource> {
        return dataSource.retrieveRootResourceEntity().mapRootResource()
    }
}
