//
//  HTTPError.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

enum HTTPError: Error {
    case timedOut
    case unreachable
    case notFound
    case badRequest
    case internalServerError
    case unauthorized
    case forbidden
}
