//
//  StringError.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

enum StringError: Error {
    case couldNotMapToURL(href: String)
    case couldNotHTMLFormatAttributedString
}
