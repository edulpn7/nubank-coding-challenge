//
//  RAMError.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

enum RAMError: Error {
    case couldNotRetrieveStoredValue
}
