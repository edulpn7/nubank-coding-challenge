//
//  ObservableType+HTTPErrorCatcher.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension HTTPError {
    init(mapping urlError: URLError) throws {
        switch urlError.code.rawValue {
        case -1001:
            self = .timedOut
        case -1009:
            self = .unreachable
        case 404:
            self = .notFound
        case 400:
            self = .badRequest
        case 500:
            self = .internalServerError
        case 401:
            self = .unauthorized
        case 403:
            self = .forbidden
        default:
            throw urlError
        }
    }
    
    init(mapping moyaError: MoyaError) throws {
        switch moyaError {
        case .statusCode(let response):
            switch response.statusCode {
            case -1001:
                self = .timedOut
            case -1009:
                self = .unreachable
            case 404:
                self = .notFound
            case 400:
                self = .badRequest
            case 500:
                self = .internalServerError
            case 401:
                self = .unauthorized
            case 403:
                self = .forbidden
            default:
                throw moyaError
            }
        case .underlying(let error, _):
            guard let urlError = error as? URLError else {
                throw error
            }
            self = try HTTPError(mapping: urlError)
        default:
            throw moyaError
        }
    }
}

extension ObservableType where E == Response {
    func catchHTTPError() -> Observable<Response> {
        return catchError { error -> Observable<Response> in
            switch error {
            case let urlError as URLError:
                guard let httpError = try? HTTPError(mapping: urlError) else {
                    return Observable.error(error)
                }
                return Observable.error(httpError)
            case let moyaError as MoyaError:
                guard let httpError = try? HTTPError(mapping: moyaError) else {
                    return Observable.error(error)
                }
                return Observable.error(httpError)
            default:
                return Observable.error(error)
            }
        }
    }
}
