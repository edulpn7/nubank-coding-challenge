//
//  NoticeResourceEntity.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct NoticeResourceEntity {
    let title: String
    let description: String
    let primaryAction: NoticeResourceActionEntity
    let secondaryAction: NoticeResourceActionEntity
    let links: NoticeResourceLinksEntity
    
    struct NoticeResourceActionEntity {
        let title: String
        let action: String
    }
    
    struct NoticeResourceLinksEntity {
        let chargebackLink: ResourceLinkEntity
    }
}
