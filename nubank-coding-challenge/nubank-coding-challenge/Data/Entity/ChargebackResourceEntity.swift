//
//  ChargebackResourceEntity.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct ChargebackResourceEntity {
    let commentHint: String
    let id: String
    let title: String
    let shouldAutoblock: Bool
    let reasonDetails: [ChargebackResourceReasonDetailsEntity]
    let links: ChargebackResourceLinksEntity
    
    struct ChargebackResourceReasonDetailsEntity {
        let id: String
        let title: String
    }
    
    struct ChargebackResourceLinksEntity {
        let blockCardLink: ResourceLinkEntity
        let unblockCardLink: ResourceLinkEntity
        let selfLink: ResourceLinkEntity
    }
}
