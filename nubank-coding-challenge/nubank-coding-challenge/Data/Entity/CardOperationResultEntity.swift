//
//  CardOperationResultEntity.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/17/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct CardOperationResultEntity {
    let status: String
}
