//
//  ChargebackResourceMoyaDataSource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya
import RxSwift

struct ChargebackResourceMoyaDataSource: ChargebackResourceDataSourceProtocol, RxMoyaDataSource {
    let provider = MoyaProvider<NubankAPI>()
    
    func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResourceEntity> {
        return request(NubankAPI.retrieveChargeback(href: href)).mapDecodableEntity()
    }
    
    func performChargeback(from href: URL, with comment: String, and reasonDetails: [(id: String, response: Bool)]) -> Observable<ChargebackResultEntity> {
        return request(.performChargeback(href: href, comment: comment, reasonDetails: reasonDetails)).mapDecodableEntity()
    }
    
    func blockCard(at href: URL) -> Observable<CardOperationResultEntity> {
        return request(NubankAPI.blockCard(href: href)).mapDecodableEntity()
    }
    
    func unblockCard(at href: URL) -> Observable<CardOperationResultEntity> {
        return request(NubankAPI.unblockCard(href: href)).mapDecodableEntity()
    }
}
