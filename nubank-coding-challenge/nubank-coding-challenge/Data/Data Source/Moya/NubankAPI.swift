//
//  NubankAPI.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/4/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya

enum NubankAPI {
    case root
    case notice(href: URL)
    case retrieveChargeback(href: URL)
    case performChargeback(href: URL, comment: String, reasonDetails: [(id: String, response: Bool)])
    case blockCard(href: URL)
    case unblockCard(href: URL)
}

extension NubankAPI: TargetType {
    var baseURL: URL {
        switch self {
        case .root:
            return URL.Nubank.HARoot
        case .notice(let href):
            return href
        case .retrieveChargeback(let href):
            return href
        case .performChargeback(let href, _, _):
            return href
        case .blockCard(let href):
            return href
        case .unblockCard(let href):
            return href
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var method: Moya.Method {
        switch self {
        case .retrieveChargeback:
            return .get
        case .blockCard, .unblockCard, .performChargeback:
            return .post
        default:
            return .get
        }
    }
    
    var path: String {
        return ""
    }
    
    //Good place to fake API data
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .performChargeback(_ , let comment, let reasonDetails):
            var parameters: [String: Any] = [:]
            parameters["comment"] = comment
            parameters["reason_details"] = reasonDetails.map { reasonDetail -> [String: Any] in
                return ["id": reasonDetail.id, "response": reasonDetail.response]
            }
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
}
