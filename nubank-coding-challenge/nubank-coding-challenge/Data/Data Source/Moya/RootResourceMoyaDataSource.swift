//
//  RootResourceMoyaDataSource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya
import RxSwift

struct RootResourceMoyaDataSource: RootResourceDataSourceProtocol, RxMoyaDataSource {
    let provider = MoyaProvider<NubankAPI>()
    
    func retrieveRootResourceEntity() -> Observable<RootResourceEntity> {
        return request(NubankAPI.root).mapDecodableEntity()
    }
}
