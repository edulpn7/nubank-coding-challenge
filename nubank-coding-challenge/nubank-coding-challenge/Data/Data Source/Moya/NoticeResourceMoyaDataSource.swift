//
//  NoticeResourceMoyaDataSource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya
import RxSwift

struct NoticeResourceMoyaDataSource: NoticeResourceDataSourceProtocol, RxMoyaDataSource {
    let provider = MoyaProvider<NubankAPI>()
    
    func retrieveNoticeResourceEntity(from href: URL) -> Observable<NoticeResourceEntity> {
        return request(NubankAPI.notice(href: href)).mapDecodableEntity()
    }
}
