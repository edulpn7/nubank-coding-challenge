//
//  ChargebackResourceEntity+DecodableMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension ChargebackResourceEntity: Decodable {
    enum ChargebackResourceEntityCodingKeys: String, CodingKey {
        case commentHint = "comment_hint"
        case id = "id"
        case title = "title"
        case autoblock = "autoblock"
        case reasonDetails = "reason_details"
        case links = "links"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ChargebackResourceEntityCodingKeys.self)
        commentHint = try container.decode(String.self, forKey: .commentHint)
        id = try container.decode(String.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        shouldAutoblock = try container.decode(Bool.self, forKey: .autoblock)
        reasonDetails = try container.decode([ChargebackResourceReasonDetailsEntity].self, forKey: .reasonDetails)
        links = try container.decode(ChargebackResourceLinksEntity.self, forKey: .links)
    }
}

extension ChargebackResourceEntity.ChargebackResourceReasonDetailsEntity: Decodable {
    enum ChargebackResourceReasonDetailsEntityCodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ChargebackResourceReasonDetailsEntityCodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
    }
}

extension ChargebackResourceEntity.ChargebackResourceLinksEntity: Decodable {
    enum ChargebackResourceLinksEntityCodingKeys: String, CodingKey {
        case blockCard = "block_card"
        case unblockCard = "unblock_card"
        case this = "self"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ChargebackResourceLinksEntityCodingKeys.self)
        blockCardLink = try container.decode(ResourceLinkEntity.self, forKey: .blockCard)
        unblockCardLink = try container.decode(ResourceLinkEntity.self, forKey: .unblockCard)
        selfLink = try container.decode(ResourceLinkEntity.self, forKey: .this)
    }
}
