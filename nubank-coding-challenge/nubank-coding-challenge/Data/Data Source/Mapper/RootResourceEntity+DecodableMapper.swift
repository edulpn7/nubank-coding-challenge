//
//  RootResourceEntity+DecodableMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension RootResourceEntity: Decodable {
    enum RootResourceEntityCodingKeys: String, CodingKey {
        case links = "links"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootResourceEntityCodingKeys.self)
        links = try container.decode(RootResourceLinksEntity.self, forKey: .links)
    }
}

extension RootResourceEntity.RootResourceLinksEntity: Decodable {
    enum RootResourceLinksEntityCodingKeys: String, CodingKey {
        case notice = "notice"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootResourceLinksEntityCodingKeys.self)
        noticeLink = try container.decode(ResourceLinkEntity.self, forKey: .notice)
    }
}
