//
//  ResourceLinkEntity+DecodableMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

extension ResourceLinkEntity: Decodable {
    enum ResourceLinkEntityCodingKeys: String, CodingKey {
        case href = "href"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ResourceLinkEntityCodingKeys.self)
        href = try container.decode(String.self, forKey: .href)
    }
}
