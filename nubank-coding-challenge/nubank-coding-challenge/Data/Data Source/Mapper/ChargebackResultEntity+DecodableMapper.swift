//
//  ChargebackResultEntity+DecodableMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/19/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

extension ChargebackResultEntity: Decodable {
    enum ChargebackResultEntityCodingKeys: String, CodingKey {
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ChargebackResultEntityCodingKeys.self)
        status = try container.decode(String.self, forKey: .status)
    }
}

