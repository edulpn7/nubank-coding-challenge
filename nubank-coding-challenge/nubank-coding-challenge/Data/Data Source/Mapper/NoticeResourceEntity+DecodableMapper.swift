//
//  NoticeResourceEntity+DecodableMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension NoticeResourceEntity: Decodable {
    enum NoticeResourceEntityCodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
        case primaryAction = "primary_action"
        case secondaryAction = "secondary_action"
        case links = "links"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NoticeResourceEntityCodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        description = try container.decode(String.self, forKey: .description)
        primaryAction = try container.decode(NoticeResourceEntity.NoticeResourceActionEntity.self, forKey: .primaryAction)
        secondaryAction = try container.decode(NoticeResourceEntity.NoticeResourceActionEntity.self, forKey: .secondaryAction)
        links = try container.decode(NoticeResourceEntity.NoticeResourceLinksEntity.self, forKey: .links)
    }
}

extension NoticeResourceEntity.NoticeResourceLinksEntity: Decodable {
    enum NoticeResourceLinksEntityCodingKeys: String, CodingKey {
        case chargeback = "chargeback"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NoticeResourceLinksEntityCodingKeys.self)
        chargebackLink = try container.decode(ResourceLinkEntity.self, forKey: .chargeback)
    }
}

extension NoticeResourceEntity.NoticeResourceActionEntity: Decodable {
    enum NoticeResourceActionEntityCodingKeys: String, CodingKey {
        case title = "title"
        case action = "action"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NoticeResourceActionEntityCodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        action = try container.decode(String.self, forKey: .action)
    }
}
