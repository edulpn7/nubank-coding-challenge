//
//  ChargebackResourceDataSourceProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol ChargebackResourceDataSourceProtocol {
    func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResourceEntity>
    func performChargeback(from href: URL, with comment: String, and reasonDetails: [(id: String, response: Bool)]) -> Observable<ChargebackResultEntity>
    func blockCard(at href: URL) -> Observable<CardOperationResultEntity>
    func unblockCard(at href: URL) -> Observable<CardOperationResultEntity>
}
