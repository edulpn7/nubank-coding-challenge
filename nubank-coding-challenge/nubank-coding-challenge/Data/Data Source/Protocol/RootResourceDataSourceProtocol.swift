//
//  RootResourceDataSourceProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol RootResourceDataSourceProtocol {
    func retrieveRootResourceEntity() -> Observable<RootResourceEntity>
}
