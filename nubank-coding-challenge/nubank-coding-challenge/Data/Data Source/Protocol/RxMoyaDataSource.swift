//
//  RxMoyaDataSource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/17/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya
import RxSwift

protocol RxMoyaDataSource {
    associatedtype T: TargetType
    
    var provider: MoyaProvider<T> {get}
    func request(_ token: T) -> Observable<Response>
}

extension RxMoyaDataSource {
    func request(_ token: T) -> Observable<Response> {
        return provider.rx.request(token)
            .asObservable()
            .debug()
            .filterSuccessfulStatusCodes()
            .catchHTTPError()
    }
}
