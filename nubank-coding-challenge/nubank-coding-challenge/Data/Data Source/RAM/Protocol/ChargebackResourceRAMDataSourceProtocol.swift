//
//  ChargebackResourceRAMDataSourceProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/18/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

protocol ChargebackResourceRAMDataSourceProtocol: class {
    var storedData: ChargebackResourceEntity? {get set}
}
