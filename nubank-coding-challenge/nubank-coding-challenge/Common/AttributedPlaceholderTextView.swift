//
//  AttributedPlaceholderTextView.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/19/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class AttributedPlaceholderTextView: UITextView {
    let disposeBag = DisposeBag()
    private var placeholderLabel: UILabel
    
    required init?(coder aDecoder: NSCoder) {
        placeholderLabel = UILabel()
        placeholderLabel.numberOfLines = 0
        super.init(coder: aDecoder)
    }
    
    var attributedPlaceholder: NSAttributedString? {
        get {
            return placeholderLabel.attributedText
        }
        set {
            placeholderLabel.removeFromSuperview()
            superview?.addSubview(placeholderLabel)
            placeholderLabel.snp.remakeConstraints { make in
                make.top.equalTo(self.snp.top).inset(textContainerInset.top)
                make.left.equalTo(self.snp.left).inset(textContainerInset.left + textContainer.lineFragmentPadding)
                make.right.equalTo(self.snp.right).inset(textContainerInset.right)
            }
            rx.text.map { text in
                guard let text = text else {
                    return true
                }
                return !text.isEmpty
            }.bind(to: placeholderLabel.rx.isHidden).disposed(by: disposeBag)
            placeholderLabel.attributedText = newValue
        }
    }
}
