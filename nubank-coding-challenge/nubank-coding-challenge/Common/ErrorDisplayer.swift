//
//  ErrorDisplayer.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/11/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol ErrorDisplayer {
    func display(_ error: Error)
}

extension ErrorDisplayer where Self: UIViewController {
    func display(_ error: Error) {
        let alert = UIAlertController(title: nil, message: R.string.localized.genericErrorMessage(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: R.string.localized.ok(), style: .cancel))
        navigationController?.present(alert, animated: true)
    }
}
