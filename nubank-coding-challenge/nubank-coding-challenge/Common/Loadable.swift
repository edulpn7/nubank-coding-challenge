//
//  Loadable.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/11/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol Loadable {
    var loadingView: UIView! {get}
    
    func startLoading()
    func stopLoading()
}

extension Loadable where Self: UIViewController {
    func startLoading() {
        if !view.subviews.contains(loadingView) {
            view.addSubview(loadingView)
        }
        if loadingView.isHidden {
            loadingView.isHidden = false
            view.bringSubview(toFront: loadingView)
        }
        loadingView.setNeedsUpdateConstraints()
    }
    
    func stopLoading() {
        if !loadingView.isHidden {
            loadingView.isHidden = true
        }
        loadingView.setNeedsUpdateConstraints()
    }
}
