//
//  String+HTMLFormatted.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

extension String {
    func HTMLFormattedAttributedString(completion: @escaping (NSAttributedString?) ->()) {
        guard let data = data(using: String.Encoding.utf16) else {
            return completion(nil)
        }
        
        DispatchQueue.main.async {
            if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                completion(attributedString)
            } else {
                completion(nil)
            }
        }
    }
}
