//
//  ObservableType+DecodableEntityMapper.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/17/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Moya
import RxSwift

extension ObservableType where E == Response {
    func mapDecodableEntity<D: Decodable>() -> Observable<D> {
        return flatMap { response -> Observable<D> in
            guard let entity = try? JSONDecoder().decode(D.self, from: response.data) else {
                return Observable.error(JSONError.couldNotMapToEntity)
            }
            
            return Observable.just(entity)
        }
    }
}
