//
//  RepositoryDependencyInjection.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct RepositoryDependencyInjection {
    static func registerRepositories(with container: Container) {
        container.register(RootResourceRepositoryProtocol.self) { resolver -> RootResourceRepository in
            return RootResourceRepository(dataSource: resolver.resolve(RootResourceDataSourceProtocol.self)!)
        }
        
        container.register(NoticeResourceRepositoryProtocol.self) { resolver -> NoticeResourceRepository in
            return NoticeResourceRepository(dataSource: resolver.resolve(NoticeResourceDataSourceProtocol.self)!)
        }
        
        container.register(ChargebackResourceRepositoryProtocol.self) { resolver -> ChargebackResourceRepository in
            return ChargebackResourceRepository(dataSource: resolver.resolve(ChargebackResourceDataSourceProtocol.self)!, ramDataSource: resolver.resolve(ChargebackResourceRAMDataSourceProtocol.self)!)
        }
    }
}
