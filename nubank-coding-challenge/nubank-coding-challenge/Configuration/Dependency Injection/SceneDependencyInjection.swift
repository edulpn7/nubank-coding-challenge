//
//  SceneDependencyInjection.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct SceneDependencyInjection {
    static func registerScenes(with container: Container) {
        NoticeConfigurator.configureNoticeScene(with: container)
        ChargebackConfigurator.configureChargebackScene(with: container)
        ChargebackConfirmationConfigurator.configureChargebackConfirmationScene(with: container)
    }
}
