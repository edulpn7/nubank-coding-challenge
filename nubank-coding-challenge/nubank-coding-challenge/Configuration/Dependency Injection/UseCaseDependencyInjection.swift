//
//  UseCaseDependencyInjection.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct UseCaseDependencyInjection {
    static func registerUseCases(with container: Container) {
        container.register(RetrieveNoticeResourceUseCaseProtocol.self) { resolver -> RetrieveNoticeResourceUseCase in
            return RetrieveNoticeResourceUseCase(rootRepository: (resolver.resolve(RootResourceRepositoryProtocol.self))!, noticeRepository: (resolver.resolve(NoticeResourceRepositoryProtocol.self))!)
        }
        
        container.register(RetrieveChargebackResourceUseCaseProtocol.self) { resolver -> RetrieveChargebackResourceUseCase in
            return RetrieveChargebackResourceUseCase(repository: resolver.resolve(ChargebackResourceRepositoryProtocol.self)!)
        }
        
        container.register(BlockCardUseCaseProtocol.self) { resolver -> BlockCardUseCase in
            return BlockCardUseCase(repository: resolver.resolve(ChargebackResourceRepositoryProtocol.self)!)
        }
        
        container.register(UnblockCardUseCaseProtocol.self) { resolver -> UnblockCardUseCase in
            return UnblockCardUseCase(repository: resolver.resolve(ChargebackResourceRepositoryProtocol.self)!)
        }
        
        container.register(PerformChargebackUseCaseProtocol.self) { resolver -> PerformChargebackUseCase in
            return PerformChargebackUseCase(repository: resolver.resolve(ChargebackResourceRepositoryProtocol.self)!)
        }
    }
}
