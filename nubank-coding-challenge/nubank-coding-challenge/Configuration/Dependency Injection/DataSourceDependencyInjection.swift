//
//  DataSourceDependencyInjection.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import Swinject

struct DataSourceDependencyInjection {
    static func registerDataSources(with container: Container) {
        container.register(RootResourceDataSourceProtocol.self) { resolver -> RootResourceMoyaDataSource in
            return RootResourceMoyaDataSource()
        }
        
        container.register(NoticeResourceDataSourceProtocol.self) { resolver -> NoticeResourceMoyaDataSource in
            return NoticeResourceMoyaDataSource()
        }
        
        container.register(ChargebackResourceDataSourceProtocol.self) { resolver -> ChargebackResourceMoyaDataSource in
            return ChargebackResourceMoyaDataSource()
        }
        
        container.register(ChargebackResourceRAMDataSourceProtocol.self) { resolver -> ChargebackResourceRAMDataSource in
            return ChargebackResourceRAMDataSource()
        }.inObjectScope(.weak)
    }
}
