//
//  URL+HARoot.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/4/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

extension URL {
    struct Nubank {
        static let HARoot = URL(string: "https://nu-mobile-hiring.herokuapp.com/")!
    }
}
