//
//  RetrieveNoticeResourceUseCase.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct RetrieveNoticeResourceUseCase: RetrieveNoticeResourceUseCaseProtocol {
    let rootRepository: RootResourceRepositoryProtocol
    let noticeRepository: NoticeResourceRepositoryProtocol
    
    func retrieveNoticeResource() -> Observable<NoticeResource> {
        return rootRepository.retrieveRootResource().flatMap { rootResource -> Observable<NoticeResource> in
            return self.noticeRepository.retrieveNoticeResource(from: rootResource.links.noticeLink.href)
        }
    }
}
