//
//  PerformChargebackUseCaseProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/19/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol PerformChargebackUseCaseProtocol {
    func performChargeback(at chargebackHref: URL, with request: PerformChargebackRequest) -> Observable<ChargebackResult>
}
