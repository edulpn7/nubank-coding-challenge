//
//  UnblockCardUseCaseProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/18/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol UnblockCardUseCaseProtocol {
    func unblockCard(at chargebackHref: URL) -> Observable<CardOperationResult>
}
