//
//  RetrieveChargebackResourceUseCaseProtocol.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol RetrieveChargebackResourceUseCaseProtocol {
    func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResource>
}
