//
//  PerformChargebackUseCase.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/19/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct PerformChargebackUseCase: PerformChargebackUseCaseProtocol {
    let repository: ChargebackResourceRepositoryProtocol
    
    func performChargeback(at chargebackHref: URL, with request: PerformChargebackRequest) -> Observable<ChargebackResult> {
        let reasonDetails = request.reasonDetails.map { reasonDetail -> (id: String, response: Bool) in
            return (id: reasonDetail.id, response: reasonDetail.response)
        }
        return repository.retrieveChargebackResource(from: chargebackHref).flatMap { chargebackResource -> Observable<ChargebackResult> in
            return self.repository.performChargeback(from: chargebackResource.links.selfLink.href, with: request.comment, and: reasonDetails)
        }
    }
}
