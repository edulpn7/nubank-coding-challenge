//
//  BlockCardUseCase.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/18/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct BlockCardUseCase: BlockCardUseCaseProtocol {
    let repository: ChargebackResourceRepositoryProtocol
    
    func blockCard(at chargebackHref: URL) -> Observable<CardOperationResult> {
        return repository.retrieveChargebackResource(from: chargebackHref).flatMap { chargebackResource -> Observable<CardOperationResult> in
            return self.repository.blockCard(at: chargebackResource.links.blockCardLink.href)
        }
    }
}
