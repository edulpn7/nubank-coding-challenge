//
//  RetrieveChargebackResourceUseCase.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

struct RetrieveChargebackResourceUseCase: RetrieveChargebackResourceUseCaseProtocol {
    let repository: ChargebackResourceRepositoryProtocol
    
    func retrieveChargebackResource(from href: URL) -> Observable<ChargebackResource> {
        return repository.retrieveChargebackResource(from: href).flatMap { chargebackResource -> Observable<ChargebackResource> in
            if chargebackResource.shouldAutoblock {
                return self.repository.blockCard(at: chargebackResource.links.blockCardLink.href).flatMap { _ -> Observable<ChargebackResource> in
                    return Observable.just(chargebackResource)
                }
            } else {
                return Observable.just(chargebackResource)
            }
        }
    }
}
