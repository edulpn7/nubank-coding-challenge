//
//  PerformChargebackRequest.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/19/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct PerformChargebackRequest {
    let comment: String
    let reasonDetails: [ChargebackRequestReasonDetail]
    
    struct ChargebackRequestReasonDetail {
        let id: String
        let response: Bool
    }
}
