//
//  ChargebackResource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/12/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct ChargebackResource {
    let commentHint: String
    let id: String
    let title: String
    let shouldAutoblock: Bool
    let reasonDetails: [ChargebackResourceReasonDetail]
    let links: ChargebackResourceLinks
    
    struct ChargebackResourceReasonDetail {
        let id: String
        let title: String
    }
    
    struct ChargebackResourceLinks {
        let blockCardLink: ResourceLink
        let unblockCardLink: ResourceLink
        let selfLink: ResourceLink
    }
}
