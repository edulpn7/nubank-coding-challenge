//
//  RootResource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct RootResource {
    let links: RootResourceLinks
    
    struct RootResourceLinks {
        let noticeLink: ResourceLink
    }
}
