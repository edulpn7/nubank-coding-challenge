//
//  ResourceLink.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct ResourceLink {
    let href: URL
}
