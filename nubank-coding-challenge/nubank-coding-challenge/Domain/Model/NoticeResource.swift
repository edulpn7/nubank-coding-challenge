//
//  NoticeResource.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/9/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct NoticeResource {
    let title: String
    let description: String
    let primaryAction: NoticeResourceAction
    let secondaryAction: NoticeResourceAction
    let links: NoticeResourceLinks
    
    
    struct NoticeResourceAction {
        let title: String
        let action: NoticeResourceActionType
        
        enum NoticeResourceActionType {
            case proceed
            case cancel
            case unknown
        }
    }
    
    struct NoticeResourceLinks {
        let chargebackLink: ResourceLink
    }
}
