//
//  NoticeResourceViewModel.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/11/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

struct NoticeResourceViewModel {
    let title: String
    let description: NSAttributedString
    let primaryAction: NoticeResourceActionViewModel
    let secondaryAction: NoticeResourceActionViewModel
    
    init(mapping model: NoticeResource, and attributedDescription: NSAttributedString) {
        title = model.title
        let correctFontDescription = NSMutableAttributedString(attributedString: attributedDescription)
        correctFontDescription.replaceFont(with: UIFont.systemFont(ofSize: 17.0))
        description = correctFontDescription
        primaryAction = NoticeResourceActionViewModel(title: model.primaryAction.title.uppercased(), href: model.links.chargebackLink.href)
        secondaryAction = NoticeResourceActionViewModel(title: model.secondaryAction.title.uppercased(), href: nil)
    }
    
    struct NoticeResourceActionViewModel {
        let title: String
        let href: URL?
    }
}
