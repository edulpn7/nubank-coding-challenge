//
//  NoticeViewController.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol NoticeViewProtocol: class, ErrorDisplayer, Loadable {
    var presenter: NoticePresenterProtocol! {get set}
    var router: NoticeRouterProtocol! {get set}
    
    func display(_ noticeResourceViewModel: NoticeResourceViewModel)
}

class NoticeViewController: UIViewController {
    var presenter: NoticePresenterProtocol!
    var router: NoticeRouterProtocol!
    @IBOutlet var loadingView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var primaryActionButton: UIButton!
    @IBOutlet weak var secondaryActionButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    var viewModel: NoticeResourceViewModel?
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeContentView()
        presenter.retrieveNotice()
    }
    
    func customizeContentView() {
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = true
    }
    
    @IBAction func primaryActionButtonTouched(_ sender: Any) {
        guard let viewModel = viewModel, let chargebackLink = viewModel.primaryAction.href else {
            return
        }
        router.navigateToChargebackScene(with: chargebackLink)
    }
}

// MARK:- Display logic
extension NoticeViewController: NoticeViewProtocol {
    func display(_ noticeResourceViewModel: NoticeResourceViewModel) {
        viewModel = noticeResourceViewModel
        titleLabel.text = noticeResourceViewModel.title
        descriptionLabel.attributedText = noticeResourceViewModel.description
        primaryActionButton.setTitle(noticeResourceViewModel.primaryAction.title, for: .normal)
        secondaryActionButton.setTitle(noticeResourceViewModel.secondaryAction.title, for: .normal)
        contentView.isHidden = false
    }
}
