//
//  NoticePresenter.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol NoticePresenterProtocol: class {
    var view: NoticeViewProtocol! {get set}
    var retrieveNoticeResourceUseCase: RetrieveNoticeResourceUseCaseProtocol! {get set}
    
    func retrieveNotice()
}

class NoticePresenter: NoticePresenterProtocol {
    weak var view: NoticeViewProtocol!
    var retrieveNoticeResourceUseCase: RetrieveNoticeResourceUseCaseProtocol!
    
    let disposeBag = DisposeBag()
    
    func retrieveNotice() {
        view.startLoading()
        retrieveNoticeResourceUseCase.retrieveNoticeResource().subscribe(onNext: { [weak self] noticeResource in
            noticeResource.description.HTMLFormattedAttributedString(completion: { attributedDescription in
                guard let weakSelf = self else {
                    return
                }
                weakSelf.view.stopLoading()
                guard let attributedDescription = attributedDescription else {
                    weakSelf.view.display(StringError.couldNotHTMLFormatAttributedString)
                    return
                }
                let viewModel = NoticeResourceViewModel(mapping: noticeResource, and: attributedDescription)
                weakSelf.view.display(viewModel)
            })
        }, onError: { [weak self] error in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            weakSelf.view.display(error)
            
        }).disposed(by: disposeBag)
    }
}
