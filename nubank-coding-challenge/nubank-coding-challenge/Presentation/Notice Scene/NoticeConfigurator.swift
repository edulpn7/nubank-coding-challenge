//
//  NoticeConfigurator.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

struct NoticeConfigurator {
    static func configureNoticeScene(with container: Container) {
        container.storyboardInitCompleted(NoticeViewController.self) { (resolver, controller) in
            let presenter = NoticePresenter()
            let router = NoticeRouter()
            presenter.view = controller
            presenter.retrieveNoticeResourceUseCase = resolver.resolve(RetrieveNoticeResourceUseCaseProtocol.self)!
            controller.router = router
            router.view = controller
            controller.presenter = presenter
        }
    }
}
