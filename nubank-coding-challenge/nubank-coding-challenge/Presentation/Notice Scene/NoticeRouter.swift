//
//  NoticeRouter.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/5/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import UIKit

protocol NoticeRouterProtocol: class {
    var view: NoticeViewProtocol! {get set}
    
    func navigateToChargebackScene(with chargebackLink: URL)
}

class NoticeRouter: NoticeRouterProtocol {
    weak var view: NoticeViewProtocol!
    
    func navigateToChargebackScene(with cashbackLink: URL) {
        guard let chargebackViewController = R.storyboard.main.chargebackViewController(), let viewController = view as? UIViewController else {
            return
        }
        chargebackViewController.presenter.link = cashbackLink
        viewController.navigationController?.pushViewController(chargebackViewController, animated: true)
    }
}
