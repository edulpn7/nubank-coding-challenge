//
//  CardOperationResultViewModel.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/18/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import Foundation

struct CardOperationResultViewModel {
    let isBlocked: Bool
    let description: String
    
    init(mapping model: CardOperationResult) {
        isBlocked = model.isBlocked
        if isBlocked {
            description = R.string.localized.blockedCardDescription()
        } else {
            description = R.string.localized.unblockedCardDescription()
        }
    }
}
