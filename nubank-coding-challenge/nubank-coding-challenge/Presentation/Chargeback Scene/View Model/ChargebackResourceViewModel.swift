//
//  ChargebackResourceViewModel.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/14/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

struct ChargebackResourceViewModel {
    let title: String
    let commentHint: NSAttributedString
    let shouldAutoblock: Bool
    let blockDescription: String
    var reasonDetails: [ChargebackResourceReasonDetailViewModel]
    
    struct ChargebackResourceReasonDetailViewModel {
        let id: String
        let title: String
        var isSelected: Bool
        
        static func array(from models: [ChargebackResource.ChargebackResourceReasonDetail]) -> [ChargebackResourceReasonDetailViewModel] {
            return models.map { model -> ChargebackResourceReasonDetailViewModel in
                return ChargebackResourceReasonDetailViewModel(mapping: model)
            }
        }
        
        init(mapping model: ChargebackResource.ChargebackResourceReasonDetail) {
            id = model.id
            title = model.title
            isSelected = false
        }
    }
    
    init(mapping model: ChargebackResource, with attributedCommentHint: NSAttributedString) {
        title = model.title.uppercased()
        shouldAutoblock = model.shouldAutoblock
        if shouldAutoblock {
            blockDescription = R.string.localized.blockedCardDescription()
        } else {
            blockDescription = R.string.localized.unblockedCardDescription()
        }
        reasonDetails = ChargebackResourceReasonDetailViewModel.array(from: model.reasonDetails)
        let correctFontCommentHint = NSMutableAttributedString(attributedString: attributedCommentHint)
        correctFontCommentHint.replaceFont(with: UIFont.systemFont(ofSize: 15.0))
        correctFontCommentHint.addAttribute(.foregroundColor, value: UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: correctFontCommentHint.length))
        commentHint = correctFontCommentHint
    }
}
