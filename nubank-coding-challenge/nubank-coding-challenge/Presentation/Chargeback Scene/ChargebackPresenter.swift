//
//  ChargebackPresenter.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import RxSwift

protocol ChargebackPresenterProtocol: class {
    var view: ChargebackViewProtocol! {get set}
    var retrieveChargebackResourceUseCase: RetrieveChargebackResourceUseCaseProtocol! {get set}
    var blockCardUseCase: BlockCardUseCaseProtocol! {get set}
    var unblockCardUseCase: UnblockCardUseCaseProtocol! {get set}
    var performChargebackUseCase: PerformChargebackUseCaseProtocol! {get set}
    var link: URL! {get set}
    
    func retrieveChargeback()
    func blockCard()
    func unblockCard()
    func performChargeback(with comment: String, and reasonDetails: [ChargebackResourceViewModel.ChargebackResourceReasonDetailViewModel])
}

class ChargebackPresenter: ChargebackPresenterProtocol {
    weak var view: ChargebackViewProtocol!
    var link: URL!
    var retrieveChargebackResourceUseCase: RetrieveChargebackResourceUseCaseProtocol!
    var blockCardUseCase: BlockCardUseCaseProtocol!
    var unblockCardUseCase: UnblockCardUseCaseProtocol!
    var performChargebackUseCase: PerformChargebackUseCaseProtocol!
    let disposeBag = DisposeBag()
    
    func retrieveChargeback() {
        view.startLoading()
        retrieveChargebackResourceUseCase.retrieveChargebackResource(from: link).subscribe(onNext: { [weak self] chargebackResource in
            guard let weakSelf = self else {
                return
            }
            chargebackResource.commentHint.HTMLFormattedAttributedString(completion: { attributedCommentHint in
                weakSelf.view.stopLoading()
                guard let attributedCommentHint = attributedCommentHint else {
                    weakSelf.view.display(StringError.couldNotHTMLFormatAttributedString)
                    return
                }
                let viewModel = ChargebackResourceViewModel(mapping: chargebackResource, with: attributedCommentHint)
                weakSelf.view.display(viewModel)
            })
            
        }, onError: { [weak self] error in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            weakSelf.view.display(error)
            
        }).disposed(by: disposeBag)
    }
    
    func blockCard() {
        view.startLoading()
        blockCardUseCase.blockCard(at: link).subscribe(onNext: { [weak self] cardOperationResult in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            let viewModel = CardOperationResultViewModel(mapping: cardOperationResult)
            weakSelf.view.display(viewModel)
            
        }, onError: { [weak self] error in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            weakSelf.view.display(error)
        }).disposed(by: disposeBag)
    }
    
    func unblockCard() {
        view.startLoading()
        unblockCardUseCase.unblockCard(at: link).subscribe(onNext: { [weak self] cardOperationResult in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            let viewModel = CardOperationResultViewModel(mapping: cardOperationResult)
            weakSelf.view.display(viewModel)
            
            }, onError: { [weak self] error in
                guard let weakSelf = self else {
                    return
                }
                weakSelf.view.stopLoading()
                weakSelf.view.display(error)
        }).disposed(by: disposeBag)
    }
    
    func performChargeback(with comment: String, and reasonDetails: [ChargebackResourceViewModel.ChargebackResourceReasonDetailViewModel]) {
        view.startLoading()
        let requestDetails = reasonDetails.map { viewModel -> PerformChargebackRequest.ChargebackRequestReasonDetail in
            return PerformChargebackRequest.ChargebackRequestReasonDetail(id: viewModel.id, response: viewModel.isSelected)
        }
        let request = PerformChargebackRequest(comment: comment, reasonDetails: requestDetails)
        performChargebackUseCase.performChargeback(at: link, with: request).subscribe(onNext: { [weak self] chargebackResult in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            weakSelf.view.displayChargebackConfirmation()
            
        }, onError: { [weak self] error in
            guard let weakSelf = self else {
                return
            }
            weakSelf.view.stopLoading()
            weakSelf.view.display(error)
            
        }).disposed(by: disposeBag)
    }
}
