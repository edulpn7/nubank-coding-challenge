//
//  ChargebackRouter.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import UIKit

protocol ChargebackRouterProtocol: class {
    var view: ChargebackViewProtocol! {get set}
    
    func navigateToNoticeScene()
    func navigateToChargebackConfirmationScene()
}

class ChargebackRouter: ChargebackRouterProtocol {
    weak var view: ChargebackViewProtocol!
    
    func navigateToNoticeScene() {
        guard let viewController = view as? UIViewController, let navigationController = viewController.navigationController else  {
            return
        }
        navigationController.popViewController(animated: true)
    }
    
    func navigateToChargebackConfirmationScene() {
        guard let viewController = view as? UIViewController, let navigationController = viewController.navigationController, let confirmationViewController = R.storyboard.main.chargebackConfirmationViewController() else  {
            return
        }
        navigationController.pushViewController(confirmationViewController, animated: true)
    }
}
