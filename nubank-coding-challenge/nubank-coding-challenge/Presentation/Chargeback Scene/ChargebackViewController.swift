//
//  ChargebackViewController.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol ChargebackViewProtocol: class, Loadable, ErrorDisplayer {
    var presenter: ChargebackPresenterProtocol! {get set}
    var router: ChargebackRouterProtocol! {get set}
    
    func display(_ chargebackResourceViewModel: ChargebackResourceViewModel)
    func display(_ cardOperationResultViewModel: CardOperationResultViewModel)
    func displayChargebackConfirmation()
}

class ChargebackViewController: UIViewController {
    var presenter: ChargebackPresenterProtocol!
    var router: ChargebackRouterProtocol!
    
    @IBOutlet var loadingView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var blockDescritptionLabel: UILabel!
    @IBOutlet weak var lockImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reasonDetailsTableView: UITableView!
    @IBOutlet weak var commentTextView: AttributedPlaceholderTextView!
    @IBOutlet weak var lockCardButton: UIButton!
    @IBOutlet weak var unlockCardButton: UIButton!
    
    var viewModel: ChargebackResourceViewModel?
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.retrieveChargeback()
        setupReasonDetailsTableView()
        customizeContentView()
    }
    
    func customizeContentView() {
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = true
    }

    
    func setupReasonDetailsTableView() {
        reasonDetailsTableView.delegate = self
        reasonDetailsTableView.dataSource = self
        reasonDetailsTableView.register(R.nib.reasonDetailTableViewCell)
    }
    
    @IBAction func chargebackButtonTouched(_ sender: Any) {
        guard let viewModel = viewModel else {
            return
        }
        presenter.performChargeback(with: commentTextView.text, and: viewModel.reasonDetails)
    }
    
    @IBAction func cancelButtonTouched(_ sender: Any) {
        router.navigateToNoticeScene()
    }
    
    @IBAction func unlockButtonTouched(_ sender: Any) {
        presenter.blockCard()
    }
    
    @IBAction func lockButtonTouched(_ sender: Any) {
        presenter.unblockCard()
    }
}

extension ChargebackViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.reasonDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel, let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.reasonDetailTableViewCell, for: indexPath) else {
            return UITableViewCell(frame: .zero)
        }
        let reasonDetail = viewModel.reasonDetails[indexPath.row]
        cell.configure(with: reasonDetail.title, isSwitchOn: reasonDetail.isSelected, and: indexPath)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}

extension ChargebackViewController: ReasonDetailTableViewCellDelegate {
    func reasonDetailTableViewCell(_ cell: ReasonDetailTableViewCell, didChangeSwitchValueTo switchState: Bool, at indexPath: IndexPath) {
        self.viewModel?.reasonDetails[indexPath.row].isSelected = switchState
    }
}

// MARK:- Display logic
extension ChargebackViewController: ChargebackViewProtocol {
    func display(_ chargebackResourceViewModel: ChargebackResourceViewModel) {
        contentView.isHidden = false
        viewModel = chargebackResourceViewModel
        titleLabel.text = chargebackResourceViewModel.title
        if chargebackResourceViewModel.shouldAutoblock {
            lockCardButton.isHidden = false
            unlockCardButton.isHidden = true
            lockImageView.image = R.image.ic_chargeback_lock()
        } else {
            lockCardButton.isHidden = true
            unlockCardButton.isHidden = false
            lockImageView.image = R.image.ic_chargeback_unlock()
        }
        blockDescritptionLabel.text = chargebackResourceViewModel.blockDescription
        commentTextView.attributedPlaceholder = chargebackResourceViewModel.commentHint
        reasonDetailsTableView.reloadData()
    }
    
    func display(_ cardOperationResultViewModel: CardOperationResultViewModel) {
        if cardOperationResultViewModel.isBlocked {
            lockCardButton.isHidden = false
            unlockCardButton.isHidden = true
            lockImageView.image = R.image.ic_chargeback_lock()
        } else {
            lockCardButton.isHidden = true
            unlockCardButton.isHidden = false
            lockImageView.image = R.image.ic_chargeback_unlock()
        }
        blockDescritptionLabel.text = cardOperationResultViewModel.description
    }
    
    func displayChargebackConfirmation() {
        router.navigateToChargebackConfirmationScene()
    }
}
