//
//  ReasonDetailTableViewCell.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/17/18.
//  Copyright © 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol ReasonDetailTableViewCellDelegate: class {
    func reasonDetailTableViewCell(_ cell: ReasonDetailTableViewCell, didChangeSwitchValueTo switchState: Bool, at indexPath: IndexPath)
}

class ReasonDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reasonSwitch: UISwitch!
    var indexPath: IndexPath?
    weak var delegate: ReasonDetailTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        return
    }
    
    func configure(with title: String, isSwitchOn: Bool, and indexPath: IndexPath) {
        titleLabel.text = title
        self.indexPath = indexPath
    }
    
    @IBAction func switchValueChanged(_ sender: Any) {
        if reasonSwitch.isOn {
            titleLabel.textColor = UIColor(red: 65.0/255.0, green: 117.0/255.0, blue: 5.0/255.0, alpha: 1.0)
        } else {
            titleLabel.textColor = UIColor(red: 34.0/255.0, green: 34.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        }
        
        guard let delegate = delegate, let indexPath = indexPath else {
            return
        }
        delegate.reasonDetailTableViewCell(self, didChangeSwitchValueTo: reasonSwitch.isOn, at: indexPath)
    }
}
