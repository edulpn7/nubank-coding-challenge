//
//  ChargebackConfigurator.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/10/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit
import Swinject

struct ChargebackConfigurator {
    static func configureChargebackScene(with container: Container) {
        container.storyboardInitCompleted(ChargebackViewController.self) { (resolver, controller) in
            let presenter = ChargebackPresenter()
            let router = ChargebackRouter()
            presenter.retrieveChargebackResourceUseCase = resolver.resolve(RetrieveChargebackResourceUseCaseProtocol.self)!
            presenter.blockCardUseCase = resolver.resolve(BlockCardUseCaseProtocol.self)!
            presenter.unblockCardUseCase = resolver.resolve(UnblockCardUseCaseProtocol.self)!
            presenter.performChargebackUseCase = resolver.resolve(PerformChargebackUseCaseProtocol.self)!
            presenter.view = controller
            controller.router = router
            router.view = controller
            controller.presenter = presenter
        }
    }
}
