//
//  ChargebackConfirmationViewController.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/20/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit

protocol ChargebackConfirmationViewProtocol: class {
    var router: ChargebackConfirmationRouterProtocol! {get set}
}

class ChargebackConfirmationViewController: UIViewController {
    var router: ChargebackConfirmationRouterProtocol!
    
    @IBOutlet weak var contentView: UIView!
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeContentView()
    }
    
    func customizeContentView() {
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = true
    }
    
    @IBAction func closeButtonTouched(_ sender: Any) {
        router.navigateToNoticeScene()
    }
}

// MARK:- Display logic
extension ChargebackConfirmationViewController: ChargebackConfirmationViewProtocol {

}
