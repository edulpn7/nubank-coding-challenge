//
//  ChargebackConfirmationConfigurator.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/20/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import UIKit
import Swinject

struct ChargebackConfirmationConfigurator {
    static func configureChargebackConfirmationScene(with container: Container) {
        container.storyboardInitCompleted(ChargebackConfirmationViewController.self) { (resolver, controller) in
            let router = ChargebackConfirmationRouter()
            router.view = controller
            controller.router = router
        }
    }
}
