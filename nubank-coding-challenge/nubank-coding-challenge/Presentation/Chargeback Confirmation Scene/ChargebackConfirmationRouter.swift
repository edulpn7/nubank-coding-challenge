//
//  ChargebackConfirmationRouter.swift
//  nubank-coding-challenge
//
//  Created by Eduardo Pinto on 4/20/18.
//  Copyright (c) 2018 Eduardo Pinto. All rights reserved.
//

import Foundation
import UIKit

protocol ChargebackConfirmationRouterProtocol: class {
    weak var view: ChargebackConfirmationViewProtocol! {get set}
    
    func navigateToNoticeScene()
}

class ChargebackConfirmationRouter: ChargebackConfirmationRouterProtocol {
    weak var view: ChargebackConfirmationViewProtocol!
    
    func navigateToNoticeScene() {
        guard let viewController = view as? UIViewController, let navigationController = viewController.navigationController else {
            return
        }
        navigationController.popToRootViewController(animated: true)
    }
}
