# nubank-coding-challenge
* Project Owner: Eduardo Lourenço Pinto Neto
* iOS Version Used: 11.2
* Xcode Version Used: 9.2
* CocoaPods Version Used: 1.5.0

This exercise uses CocoaPods as dependecy management tool. Make sure you have it installed and up to date by running ```gem install cocoapods```. With Cocoapods installed and updated, navigate to the Xcode project root folder (the folder where you can find the ```Podfile``` file) and run ```pod install```. 

After that you should be set, so just open ```nubank-coding-challenge.xcworkspace``` on Xcode and run the project on a device or simulator.

## Open Source Libraries Used ##
* Moya/RxSwift (11.0.1)
* RxCocoa (4.1.2)
* SnapKit (4.0.0)
* Swinject (2.2.0)
* SwinjectStoryboard (1.1.2)
* R.swift  (4.0.0)
* RxNimble (4.1.0)


